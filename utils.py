# -*- coding: utf-8 -*-
#!/usr/bin/env python3


def convert_seconds(time):
    day = time // (24 * 3600)
    time = time % (24 * 3600)
    hour = time // 3600
    time %= 3600
    minutes = time // 60
    time %= 60
    seconds = time
    return f"{day} day(s) {hour} hour(s) {minutes} min(s) {seconds} sec(s)"
