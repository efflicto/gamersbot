# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import requests


class ICanHazDadJoke():

    def __init__(self):
        self.session = requests.session()

    def get_joke(self):
        headers = {"Accept": "text/plain"}
        return self.session.get(url="https://icanhazdadjoke.com/", headers=headers).text
