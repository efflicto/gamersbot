# -*- coding: utf-8 -*-
#!/usr/bin/env python3

import requests


class DivisionLab():

    def __init__(self, logger=None):
        if logger:
            self.logger = logger

        self.url_search = "https://thedivisiontab.com/api/search.php?name={}&platform={}"
        self.url_player = "https://thedivisiontab.com/api/player.php?pid={}"
        self.session = requests.Session()

    def get_player_json(self, player_name, platform=None):
        if platform:
            platforms = ['uplay', 'psn' 'xbl']
            if platform in platforms:
                result = self.session.get(url=self.url_search.format(player_name, platform)).json()
                if not result['totalresults'] == 0:
                    return result['results']
                else:
                    return None
            else:
                return "Wrong platform."
        else:
            result = self.session.get(url=self.url_search.format(player_name, "uplay")).json()
            if not result['totalresults'] == 0:
                return result['results']
            else:
                return None

    def get_player_detail(self, pid):
        return self.session.get(url=self.url_player.format(pid)).json()

    def get_player_stats(self, player_name, platform=None):
        player_data = self.get_player_json(player_name=player_name, platform=platform)
        if player_data:
            if "Wrong platform." in player_data:
                return player_data
            elif len(player_data) > 1:
                player_names = [x['name'] for x in player_data]
                return player_names
            else:
                print(player_data[0]['pid'])
                player_stats = self.get_player_detail(pid=player_data[0]['pid'])
                if player_stats['playerfound']:
                    return player_stats
                else:
                    return False
        else:
            return "Nothing found. Sorry."
