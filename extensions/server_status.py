# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import requests
from terminaltables import AsciiTable


class ServerStatus():

    def __init__(self):
        self.status_url = "https://game-status-api.ubisoft.com/v1/instances?appIds=6c6b8cd7-d901-4cd5-8279-07ba92088f06,6f220906-8a24-4b6a-a356-db5498501572,7d9bbf16-d76d-43e1-9e82-1e64b4dd5543"
        self.session = requests.session()

    def get_status(self):
        status = self.session.get(self.status_url).json()
        return status

    def get_status_table(self):
        status = self.get_status()
        platform_status = [["Platform", "Status", "Maintenance"]]
        for x in status:
            platform_status.append(
                [
                    f"{x['Platform']}",
                    f"{x['Status']}",
                    f"{x['Maintenance']}"
                ])

        table = AsciiTable(platform_status)
        return "```{}```".format(table.table)
