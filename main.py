# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import logging

from bot import GamersBot
from config import discord_token

from logging.handlers import RotatingFileHandler

logger = logging.getLogger("discord")
logger.setLevel(logging.DEBUG)
handler = RotatingFileHandler('log/log.log',
                              maxBytes=1000000,
                              backupCount=10)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s [GamersBot] [%(levelname)-5.5s]  %(message)s")
handler.setFormatter(formatter)
logger.addHandler(handler)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


def main():
    client = GamersBot(logger=logger)
    client.run(discord_token)


if __name__ == '__main__':
    main()
