# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import asyncio

import discord
from datetime import datetime
from extensions.divisionlab import DivisionLab
from extensions.server_status import ServerStatus
from extensions.icanhazdadjoke import ICanHazDadJoke
from terminaltables import AsciiTable
from utils import convert_seconds


class GamersBot(discord.Client):

    def __init__(self, logger):
        super().__init__()
        self.logger = logger
        self.div_lab = DivisionLab()
        self.server_status = ServerStatus()

    async def div_player_stats(self, message, player_name, platform=None):
        player_stats = self.div_lab.get_player_stats(player_name=player_name, platform=platform)
        if type(player_stats) == list:
            player_list = '\n'.join(player_stats)
            await self.send_message(message.channel,
                                    content="Sorry I've found more than one Player with that name."
                                            "\nYou can choose one of the following names with: "
                                            "!div profile *playername* *(platform)*: "
                                            "\n```{}```".format(player_list))
        elif not type(player_stats) == dict:
            await self.send_message(message.channel, content=player_stats)
        else:
            if not player_stats['specialization'] == "":
                specialization = player_stats['specialization']
            else:
                specialization = "No specialization"
            table_data = [
                ['Name and Specc', "{} - {}".format(player_stats['name'], specialization)],
                ['Credits', player_stats['ecredits']],
                ['Level', 'PVE: {} | DZ: {}'.format(player_stats['level_pve'], player_stats['level_dz'])],
                ['Time played', convert_seconds(player_stats['timeplayed_total'])],
                ['Kills', "PVE: {} | PVP: {} | Total: {} | Headshots: {}".format(player_stats['kills_npc'],
                                                                                 player_stats['kills_pvp'],
                                                                                 player_stats['kills_total'],
                                                                                 player_stats['headshots'])],
                ['Kill types',
                 "Bleed: {} | Shock: {} | Burn: {} | Ensnare: {} | Headshot: {} | Skill: {} | Turret: {}".format(
                     player_stats['kills_bleeding'],
                     player_stats['kills_shocked'],
                     player_stats['kills_burning'],
                     player_stats['kills_ensnare'],
                     player_stats['kills_headshot'],
                     player_stats['kills_skill'],
                     player_stats['kills_turret'])],
                ['Gear', "Items looted: {} | Gearscore: {}".format(player_stats['looted'], player_stats['gearscore'])],
                ['Updated',
                 '{}(UTC)'.format(datetime.utcfromtimestamp(int(player_stats['utime'])).strftime('%Y-%m-%d %H:%M:%S'))]
            ]
            table = AsciiTable(table_data)
            stats = "```{}```".format(table.table)
            if not player_name == player_stats['name']:
                stats_message = "Haven't found exact player name {}. So here is the profile for player: {}".format(
                    player_name, player_stats['name'])
            else:
                stats_message = "Profile for player: {}".format(player_stats['name'])
            await self.send_message(message.channel, content=stats_message,
                                    embed=discord.Embed().set_image(url=player_stats['avatar_146']))
            await self.send_message(message.channel, content=stats)

    async def on_ready(self):
        self.logger.info(f'Logged on as: {self.user.name}')
        await self.change_presence(game=discord.Game(name='On Duty'))

    async def on_message(self, message):

        self.logger.debug(f"{message.channel} - {message.author}: {message.content}")

        # don't respond to ourselves
        if message.author == self.user:
            return

        if message.content.lower().startswith("!help"):
            known_commands = []
            known_commands.append(["Command", "Function"])
            known_commands.append(["!ping", "Pong!"])
            known_commands.append(["!joke", "A joke..."])
            known_commands.append(["!destroy", "DO NOT use this please."])
            known_commands.append(["!source", "Link to the source code of this bot."])
            known_commands.append(["!invite", "Creates an invite link for this server."])
            known_commands.append(["!div status", "Shows the current The Division 2 server status."])
            known_commands.append(["!div profile", "Returns the profile of an The Division 2 agent."])
            table = AsciiTable(known_commands)
            help_message = "Known commands:\n```" \
                           "{}" \
                           "```".format(table.table)
            await self.send_message(message.channel, content=help_message)

        if message.content.lower().startswith("!ping"):
            self.logger.debug(f"Got ping from {message.author.name}")
            await self.send_message(message.channel, content="Pong!")

        if message.content.lower().startswith("!source"):
            await self.send_message(message.channel,
                                    content="Here is the source: https://gitlab.com/efflicto/gamersbot")

        if message.content.lower().startswith("!div profile"):
            command = message.content.split(' ')
            if len(command) == 2:
                await self.send_message(message.channel, content="Usage:\n"
                                                                 "!div profile *username*\n"
                                                                 "Without a trailing platform name it uses uplay\n"
                                                                 "Platforms: uplay, psn, xbl\n"
                                                                 "Example:\n"
                                                                 "!div profile *username* psn")
            elif len(command) == 3:
                await self.div_player_stats(message=message, player_name=command[2], platform=None)
            elif len(command) == 4:
                await self.div_player_stats(message=message, player_name=command[2], platform=command[3])
            else:
                pass

        if message.content.lower().startswith("!invite"):
            if message.server:
                invite_link = await self.create_invite(destination=message.server, max_age=0, max_uses=0, unique=False)
                await self.send_message(message.channel, content="Here is your invite link: {}".format(invite_link))

        if message.content.lower().startswith("!div status"):
            status = self.server_status.get_status_table()
            await self.send_message(message.channel, content=status)

        if message.content.lower().startswith("!joke"):
            joke_api = ICanHazDadJoke()
            joke = "{} :joy: :joy: :joy: :ok_hand: ".format(joke_api.get_joke())
            await self.send_message(message.channel, content=joke)

        if message.content.lower().startswith("!destroy"):
            await self.send_message(message.channel, content="Self destruction in 3")
            await asyncio.sleep(1)
            await self.send_message(message.channel, content="2")
            await asyncio.sleep(1)
            await self.send_message(message.channel, content="1")
            await asyncio.sleep(3)
            await self.send_message(message.channel, content="...")
            await asyncio.sleep(1)
            await self.send_message(message.channel, embed=discord.Embed().set_image(
                url="https://media.giphy.com/media/U6pavBhRsbNbPzrwWg/giphy.gif"))
